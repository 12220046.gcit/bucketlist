const text=document.getElementById("quote");
const author=document.getElementById("author");
const tweetButton=document.getElementById("tweet");
let modalState = 'add';
var serialnumberforput;
const getNewQuote = async () =>
{
    //api for quotes
    var url="https://type.fit/api/quotes";    

    // fetch the data from api
    const response=await fetch(url);
    //convert response to json and store it in quotes array
    const allQuotes = await response.json();

    // Generates a random number between 0 and the length of the quotes array
    const indx = Math.floor(Math.random()*allQuotes.length);

    //Store the quote present at the randomly generated index
    const quote=allQuotes[indx].text;
    
    //Store the author of the respective quote
    const auth=allQuotes[indx].author;

    if(auth==null)
    {
        author.innerHTML = "Anonymous";
    }
 
    //function to dynamically display the quote and the author
    text.innerHTML=quote;
    author.innerHTML="~ "+auth;

    //tweet the quote
    tweetButton.href="https://twitter.com/intent/tweet?text="+quote+" ~ "+auth;

}

getNewQuote();


// form
window.onload = function() {
  fetch('/buckets')
      .then(response => response.text())

      .then(data => showbuckets(data))
    }  

function showbuckets(data) {
  console.log(data)

  const datas = JSON.parse(data);
    createTasks(datas);
}

// form
let form = document.getElementById("form");
let textInput = document.getElementById("textInput");
let category = document.getElementById("category")
let dateInput = document.getElementById("dateInput");
let loca = document.getElementById("loca");
let textarea = document.getElementById("textarea");
let msg = document.getElementById("msg");
let stat = document.getElementById("status")
let tasks = document.getElementById("tasks");
let add = document.getElementById("add");



let formValidation = () => {
  if (textInput.value === "") {
    console.log("failure");
    msg.innerHTML = "Task cannot be blank";
  } else {
    console.log("success");
    msg.innerHTML = "";
    acceptData();
    add.setAttribute("data-bs-dismiss", "modal");
    add.click();

    (() => {
      add.setAttribute("data-bs-dismiss", "");
    })();
  }
};

let data = [{}];
let acceptData = () => {
  console.log(textInput.value)
  data.push({
    name: textInput.value,
    categories: category.value,
    date: dateInput.value,
    location: loca.value,
    description: textarea.value,
    status: stat.value,
  });
  resetForm();
  saveBucket(data[data.length - 1]);
  location.reload();
};

let createTasks = (tasksData) => {
  tasks.innerHTML = "";
  tasksData.forEach((task) => {
    tasks.innerHTML += `
      <div id="bucketlist">
        <span id="task.name" class="fw-bold">${task.name}</span>
        <span id="task.categories" class="small text-secondary">${task.categories}</span>
        <span id="task.date" class="small text-secondary">${task.date}</span>
        <span id="task.location" class="small text-secondary">${task.location}</span>
        <p id="task.description">${task.description}</p>
        <span id="task.status" class="small text-secondary">${task.status}</span>
        <span id="serialnumber" class="small text-secondary">${task.serialNo}</span>
        <span class="options">
          <i onClick="editTask(this)" data-bs-toggle="modal" data-mode="edit" data-bs-target="#form" class="fa fa-edit"></i>
          <i onClick="deleteTask(this); createTasks()" class="fa fa-trash"></i>
        </span>
      </div>`;
  });
  
};

// let deleteTask = (e) => {
//   if(confirm("Are you sure to delete it?")){
//     e.parentElement.parentElement.remove();
//     data.splice(e.parentElement.parentElement.id, 1);
//     localStorage.setItem("data", JSON.stringify(data));
  
//   };
//   }
  let deleteTask = (e) => {
    if (confirm("Are you sure you want to delete it?")) {
      const taskId = e.parentElement.parentElement.id;
      const task = data[taskId];
      const serialNumber = e.parentElement.parentElement.querySelector("#serialnumber").innerText;
      fetch(`/bucket/${serialNumber}`, {
        method: 'DELETE'
      })
        .then(response => {
          if (response.ok) {
            e.parentElement.parentElement.remove();
            data.splice(taskId, 1);
            location.reload();
          } else {
            throw new Error('Error deleting task');
            location.reload();
          }
        })
        .catch(error => console.error(error));
    }
  };
  
  let editTask = (e) => {
    modalState = 'edit';
  const taskNameElement = e.closest("#bucketlist").querySelector("#task\\.name");
  const name = taskNameElement.textContent;

  const categoryElement = e.closest("#bucketlist").querySelector("#task\\.categories");
  const cate = categoryElement.textContent;

  const dateElement = e.closest("#bucketlist").querySelector("#task\\.date");
  const date = dateElement.textContent;

  const locationElement = e.closest("#bucketlist").querySelector("#task\\.location");
  const loc = locationElement.textContent;

  const descriptionElement = e.closest("#bucketlist").querySelector("#task\\.description");
  const des = descriptionElement.textContent;

  const statusElement = e.closest("#bucketlist").querySelector("#task\\.status");
  const status = statusElement.textContent;

  serialnumberforput = e.closest('#bucketlist').querySelector('#serialnumber').textContent
console.log(serialnumberforput)
    // Populate the form fields with the task data
    document.getElementById("textInput").value = name;
    document.getElementById("category").value = cate;
    document.getElementById("dateInput").value = date;
    document.getElementById("loca").value = loc;
    document.getElementById("textarea").value = des;
    document.getElementById("status").value = status;


    // Submit event handler for the form

  };


  form.addEventListener('submit', function (event) {
    event.preventDefault();
    if (modalState === 'edit') {
  
      const taskData = {
        // Retrieve updated values from the form fields
        name: document.getElementById('textInput').value,
        categories: document.getElementById('category').value,
        date: document.getElementById('dateInput').value,
        location: document.getElementById('loca').value,
        description: document.getElementById('textarea').value,
        status: document.getElementById('status').value
      };
      console.log(serialnumberforput)
  
      fetch(`/bucket/${serialnumberforput}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(taskData)
      })
        .then(response => {
          if (response.ok) {
            // Handle successful PATCH request
            // ...
            console.log('Bucket updated successfully');
          location.reload()
          } else {
            throw new Error('Error updating task');
          }
        })
        .catch(error => console.error(error));
    }
    else{
        formValidation();
        console.log("hii")
    }
    });


let updateBucket = (bucketData) => {
  const taskId = bucketData.id; // Assuming you have an `id` property in your bucket data

  // Prepare the updated bucket data
  const updatedBucket = {
    serialNo: bucketData.serialNo,
    name: bucketData.text,
    description: bucketData.description,
    Date: bucketData.Date,
    categories: bucketData.category,
    location: bucketData.location,
    status: bucketData.status
  };

  fetch(`/bucket/${taskId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(updatedBucket)
  })
    .then(response => {
      if (response.ok) {
        console.log('Bucket updated successfully');
        return response.json();
      } else {
        throw new Error('Error updating bucket');
      }
    })
    .then(data => {
      console.log('Updated bucket:', data);
      // Perform any necessary actions after the bucket is successfully updated
    })
    .catch(error => console.error('Error updating bucket:', error));
};

let resetForm = () => {
  textInput.value = "";
  dateInput.value = "";
  textarea.value = "";
};

function saveBucket(bucketData) {
  fetch('/bucket', {
    method: 'POST',
    body: JSON.stringify(bucketData),
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.text())
    .then(data => console.log('Bucket created:', data))
    .catch(error => console.error('Error creating bucket:', error));
}