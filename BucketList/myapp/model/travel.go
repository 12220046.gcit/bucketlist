package model

import (
	"myapp/myapp/datastore/postgres"
)


type Travel struct {
	SerialNo string
	Title string
	Image   string
	Description string
}

const queryInsertTravel = " INSERT INTO travel(serialNo,Title, Image, Description) VALUES($1,$2,$3, $4);"
const queryGetTravel = "SELECT serialNo,Title, Name, Description FROM travel WHERE serialNo=$1;"

const (
	queryDeleteTravel = "DELETE from travel where serialNo= $1;"
	queryUpdateTravel = "UPDATE travel SET serialNo=$1, Title=$2, Image=$3,Description=$4 where serialNo=$5;"
)


func (t *Travel) Create() error {
	// imageBytes, err := ioutil.ReadAll(t.Image)
	// if err != nil {
	// 	return err
	// }

	_, err := postgres.Db.Exec(queryInsertTravel, t.SerialNo,t.Title, t.Image, t.Description)
	return err
}
func (t *Travel) Get() error{
	return postgres.Db.QueryRow(queryGetTravel, t.SerialNo).Scan(&t.SerialNo,&t.Title, &t.Image, &t.Description)
}

func GetAllTravel()([]Travel, error){
	rows, getErr := postgres.Db.Query("SELECT serialNo,Title, Image, Description FROM travel")
	if getErr != nil{
		return nil, getErr
	}
	//create a slice of type Course
	travels:= []Travel{}

	for rows.Next(){
		var t Travel
		dbErr := rows.Scan(&t.SerialNo,&t.Title, &t.Image,&t.Description)
		if dbErr != nil{
			return nil, dbErr
		}
		travels = append(travels, t)

	}
	rows.Close()
	return travels, nil
}

func (b *Travel)Delete() error{
	if _, err:= postgres.Db.Exec( queryDeleteTravel,  b.SerialNo); err != nil{
		return err
	}
	return nil
}

func (b *Travel) Update(old_sn string) error {
	err := postgres.Db.QueryRow(queryUpdateTravel, b.SerialNo,b.Title, b.Image, b.Description).Scan(&b.SerialNo)
	return err
}

