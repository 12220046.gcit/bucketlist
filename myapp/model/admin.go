// package model

// import "myapp/datastore/postgres"

// type Admin struct{
// 	FirstName string
// 	LastName string
// 	Email string
// 	Password string
// }

// const queryInsertAdmin = "INSERT INTO admin(firstname, lastname, email, password) VALUES($1,$2,$3,$4)"

// func (adm *Admin) Create() error{
// 	_,err:= postgres.Db.Exec(queryInsertAdmin, adm.FirstName, adm.LastName, adm.Email, adm.Password)
// 	return err
// }

package model

import "myapp/myapp/datastore/postgres"



type Admin struct {
	Name string
	Email     string
	Password  string
}

const queryInsertAdmin = " INSERT INTO admin(name, email, password) VALUES($1,$2,$3);"
const queryGetAdmin = "SELECT name, email, password FROM admin WHERE email=$1 and password=$2;"

const (
	queryGetUser    = "Select name,email,password from admin where email=$1"
	queryDeleteUser = "DELETE from admin where email= $1;"
	queryUpdateUser = "UPDATE admin SET name=$1, email=$2,  password=$3 where email=$4;"
)


func (adm *Admin) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin,  adm.Name, adm.Email, adm.Password)
	return err
}
func (adm *Admin) Get() error{
	err := postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Name, &adm.Email, &adm.Password)
	if err != nil {
		return err
	}
	return nil
}


func (gUser *Admin) GetUsers() error {
	stmt, err := postgres.Db.Prepare(queryGetUser)
	if err != nil {
		return err
	}
	defer stmt.Close()
	result := stmt.QueryRow(gUser.Email)
	getErr := result.Scan(&gUser.Name, &gUser.Email,  &gUser.Password)
	if getErr != nil {
		return getErr
	}
	return nil
}

func (demail *Admin) Delete() error {
	stmt, err := postgres.Db.Prepare(queryDeleteUser)

	if err != nil {
		return err
	}
	defer stmt.Close()

	if _, err = stmt.Exec(demail.Email); err != nil {
		return err
	}
	return nil
}

func (regUser *Admin) Update(email string) error {
	stmt, updateErr := postgres.Db.Prepare(queryUpdateUser)
	if updateErr != nil {
		return updateErr
	}
	defer stmt.Close()

	_, updateErr = stmt.Exec(regUser.Name, regUser.Email, regUser.Password, email)

	if updateErr != nil {
		return updateErr
	}
	return nil
}

func GetUserEmail(email string) (*Admin, error) {
	result := &Admin{Email: email}
	if err := result.GetUsers(); err != nil {
		return nil, err
	}
	return result, nil
}

func GetAll() ([]Admin, error) {
	rows, getErr := postgres.Db.Query("Select * from admin;")
	if getErr != nil {
		return nil, getErr
	}
	users := []Admin{}

	for rows.Next() {
		user := Admin{}
		dbErr := rows.Scan(&user.Name, &user.Email, &user.Password)
		if dbErr != nil {
			return nil, dbErr
		}
		users = append(users, user)
	}
	rows.Close()
	return users, nil
}

func DeleteUsers(mail string) error {
	current, err := GetUserEmail(mail)
	if err != nil {
		return err
	}
	return current.Delete()
}


func UpdateUser(user Admin, email string) (*Admin, error) {
	current, err := GetUserEmail(email)
	if err != nil {
		return nil, err
	}

	current.Name = user.Name
	current.Email = user.Email
	current.Password = user.Password

	if err := current.Update(email); err != nil {
		return nil, err
	}
	return current, nil
}
