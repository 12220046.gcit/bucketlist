package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdminLogin(t *testing.T){
	url := "http://localhost:8085/signup"
	var data  = []byte(`{
	"name": "Leki",
  "email": "lolikirasama@gmail.com",
  "password": "12345"
}`)
	// create request object
	req, _ := http.NewRequest("GET", url, bytes.NewBuffer(data))
	req.Header.Set("Content-type", "application/json")
	// create a client
	client := &http.Client{}
	// send POST request using Do function
	resp, err := client.Do(req)
	if err != nil{
		panic(err)
	}
	defer resp.Body.Close()
	body,_ := io.ReadAll(resp.Body)
	// assert statement
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	// if two json are equal or not
	assert.JSONEq(t, `{"message": "success"}`, string(body))
}


// go get github.com/stretchr/testify/assert