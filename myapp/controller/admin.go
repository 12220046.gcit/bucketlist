package controller

import (
	"encoding/json"
	"myapp/myapp/model"
	"myapp/myapp/utils/httpResp"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/mux"
	// "github.com/gorilla/sessions"
)

// var store = sessions.NewCookieStore([]byte("your-secret-key"))

func Signup( w http.ResponseWriter, r *http.Request){
	var admin model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin);err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr:= admin.Create()
	if saveErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// //no error
	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"status": "admin added"})
}

func Login(w http.ResponseWriter, r *http.Request) {
	
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	// creating cookie
	cookieValue, err := json.Marshal(admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "failed to create cookie")
		return
	}
	encodedCookieValue := url.QueryEscape(string(cookieValue))
	cookie := http.Cookie{
		Name: "my-cookie",
		Value: string(encodedCookieValue), //info of the user is stored in value
		Expires: time.Now().Add(20 * time.Minute),
		Secure: true,
	}
	//set cookie and sent back to client
	http.SetCookie(w, &cookie)
	getErr := admin.Get()
	if getErr != nil {
	httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
	return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message":"success"})
}


func VerifyCookie(w http.ResponseWriter, r *http.Request) bool{
	var admin model.Admin
	cookieValue, err := json.Marshal(admin)
	encodedCookieValue := url.QueryEscape(string(cookieValue))
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "failed to create cookie")
		return false
	}
	cookie, err := r.Cookie("my-cookie")
	if err != nil{
		if err == http.ErrNoCookie{
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not set")
			return false
		}
		httpResp.RespondWithJson(w, http.StatusInternalServerError, "internal server eror")
		return false
	}

	//validate cookie value
	if cookie.Value != encodedCookieValue {
		httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie value does not match")
		return false
	}
	return true
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	user, getErr := model.GetUserEmail(email)
	cookie := http.Cookie{
		Name: "my-cookie",
		Value: string(email), //info of the user is stored in value
		Expires: time.Now().Add(20 * time.Minute),
		Secure: true,
	}
	//set cookie and sent back to client
	http.SetCookie(w, &cookie)
	if getErr != nil {
	httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
	return
	}
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, user)
}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	users, getErr := model.GetAll()

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusCreated, users)
}

func DeleteUsers(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]

	err := model.DeleteUsers(email)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "Deleted"})
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var user model.Admin
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json body")
		return
	}
	defer r.Body.Close()

	result, err := model.UpdateUser(user, email)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, result)
}

func Logout (w http.ResponseWriter, r *http.Request){
	http.SetCookie(w, &http.Cookie{
		Name: "my-cookie",
		Expires: time.Now(),
	})
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "logout success"})
}