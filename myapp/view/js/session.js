const sessionExists = !!localStorage.getItem('email');

console.log(localStorage.getItem('email')); // Check the value of 'email' in localStorage
console.log(typeof localStorage.getItem('email')); // Check the data type of the retrieved value

if (!sessionExists) {
    alert("Please Login")
    location.assign("index.html")
}